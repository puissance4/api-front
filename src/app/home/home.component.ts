import { Component, OnInit } from '@angular/core';
import { IgdbService } from '../services/igdb.service';
import { Game } from '../classes/Game';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  games: any;
  constructor(
    public igdbService: IgdbService
  ) { }

  ngOnInit(): void {
    this.getGames();
  }

  getGames() {
    this.igdbService.getGames()
      .subscribe(
        data => this.games = data
      );
  }
}
