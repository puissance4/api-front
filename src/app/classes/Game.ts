export class Game {
  constructor(
    public id?: number,
    public name?: string,
    public url?: string,
    public summary?: string
  ) {  }
}


