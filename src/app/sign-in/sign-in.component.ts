import { Component, OnInit } from '@angular/core';
import { AuthService} from '../services/auth.service';
import { User } from '../classes/User';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  model = new User(0, '', '', '');

  submitted = false;
  result;
  hiddenText = 'Vous êtes inscrit';
  onSubmit() {
    this.authService.registerUser(this.model)
      .subscribe(
      data => this.result = data,
      error => this.hiddenText = error
    );
    this.submitted = true;
  }

  constructor(
     public authService: AuthService
  ) { }

  ngOnInit(): void {
  }

}
