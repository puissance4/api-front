import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LogInComponent} from './log-in/log-in.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AuthGuard } from './interceptor';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'log-in', component: LogInComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'profil', component: ProfileSettingsComponent, canActivate: [AuthGuard]},
  { path: '*', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
