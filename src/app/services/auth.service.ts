import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../classes/User';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = '192.168.1.26:81'
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public connected: boolean;
  constructor(
    private http: HttpClient,
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.connected = false;
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  registerUser(user: User) {
    const httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    const httpOptions = {
      headers: httpHeaders
      };
    return this.http.post(`/apiGaming/users/register`, user, httpOptions)
     .pipe(
            catchError(this.handleError)
          );
  }
  login(user){
      return this.http.post<any>(`/apiGaming/users/login`, user)
        .pipe(map(data => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.currentUserSubject.next(data);
          this.connected = true;
          return data;
        }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.connected = false;
  }

  getUser(){
    return this.http.get('/apiGaming/users/profil');
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
