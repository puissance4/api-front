import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../classes/User';
import { SteamService } from '../services/steam.service';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {
  user: User;
  steamSummary: any;
  recentGames: any;
  getUser(){
    this.authService.getUser().subscribe(
      data => this.user = data
    );
  }
  getSteamSummary() {
    this.steamService.getUserSummary().subscribe(
      data => {
        this.steamSummary = data;
        this.getRecentGames(data['steamID']);
      }
    );
  }
  getRecentGames(steamid) {
    this.steamService.getRecentGames(steamid).subscribe(
      data => this.recentGames = data
    );
  }
  constructor(
    public authService: AuthService,
    public steamService: SteamService
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.getSteamSummary();
  }

}
