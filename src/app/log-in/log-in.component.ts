import { Component, OnInit } from '@angular/core';
import { AuthService} from '../services/auth.service';
import { User } from '../classes/User';
import { Router, ActivatedRoute } from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  model = new User(0, '', '', '');

  submitted = false;
  result;
  returnUrl: string = '/';
  error = '';
  hiddenText = 'Vous êtes connecté';
  onSubmit() {
    this.submitted = true;
    this.authService.login(this.model)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
        });
  }

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
  }

}
